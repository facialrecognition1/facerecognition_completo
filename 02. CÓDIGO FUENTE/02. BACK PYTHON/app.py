from flask import Flask, request, jsonify
import cv2
import os
import imutils
import numpy as np  
from entrenandoRF import train_model

app = Flask(__name__)





@app.route('/recognize_face', methods=['GET'])
def recognize_face():

    dataPath = './resources'  # Cambia a la ruta donde hayas almacenado Data
    imagePaths = os.listdir(dataPath)
    print('imagePaths=', imagePaths)
    
    face_recognizer = cv2.face.EigenFaceRecognizer_create()
    face_recognizer.read('modeloEigenFace.xml')
    cap = cv2.VideoCapture(0, cv2.CAP_DSHOW) 
    faceClassif = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
    
    recognized_names = []

    while True:
        ret, frame = cap.read()
        if not ret:
            return jsonify({"message": "No se pudo capturar la imagen"})

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        auxFrame = gray.copy()
    
        faces = faceClassif.detectMultiScale(gray, 1.3, 5)
    
        for (x, y, w, h) in faces:
            rostro = auxFrame[y:y + h, x:x + w]
            rostro = cv2.resize(rostro, (150, 150), interpolation=cv2.INTER_CUBIC)
            result = face_recognizer.predict(rostro)
    
            # EigenFaces
            if result[1] < 5700:
                recognized_name = imagePaths[result[0]]
            else:
                recognized_name = 'Desconocido'
            recognized_names.append(recognized_name)
    
            if recognized_names:
                return jsonify({"recognized_names": recognized_names})
            else:
                return jsonify({"message": "No se detectaron rostros"})



@app.route('/capture_images', methods=['POST'])
def capture_images():
    try:
        # Recupera el nombre de la persona y la ruta de almacenamiento de la solicitud POST
        data = request.get_json()
        personName = data.get('personName')
        dataPath = data.get('dataPath')

        if personName is None or dataPath is None:
            return jsonify({'error': 'Se requiere el nombre de la persona y la ruta de almacenamiento'}), 400

        personPath = os.path.join(dataPath, personName)

        if not os.path.exists(personPath):
            print('Carpeta creada: ', personPath)
            os.makedirs(personPath)

        cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        faceClassif = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
        count = 0

        while True:
            ret, frame = cap.read()
            if ret is False:
                break
            frame = imutils.resize(frame, width=640)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            auxFrame = frame.copy()

            faces = faceClassif.detectMultiScale(gray, 1.3, 5)

            for (x, y, w, h) in faces:
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                rostro = auxFrame[y:y + h, x:x + w]
                rostro = cv2.resize(rostro, (150, 150), interpolation=cv2.INTER_CUBIC)
                cv2.imwrite(os.path.join(personPath, 'rostro_{}.jpg'.format(count)), rostro)
                count += 1
            cv2.imshow('frame', frame)

            k = cv2.waitKey(1)
            if k == 27 or count >= 30:
                break

        cap.release()
        cv2.destroyAllWindows()
        
        train_model(dataPath)

        return jsonify({'message': 'Imágenes capturadas exitosamente'})
    except Exception as e:
        return jsonify({'error': str(e)}), 500

    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port='4000', debug=True)
