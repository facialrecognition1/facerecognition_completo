import cv2
import os
import numpy as np

def train_model(dataPath):
    peopleList = os.listdir(dataPath)

    labels = []
    facesData = []
    label = 0

    for nameDir in peopleList:
        personPath = dataPath + '/' + nameDir

        for fileName in os.listdir(personPath):
            labels.append(label)
            facesData.append(cv2.imread(personPath+'/'+fileName, 0))
        label = label + 1

    # Métodos para entrenar el reconocedor
    face_recognizer = cv2.face_EigenFaceRecognizer.create()

    # Entrenando el reconocedor de rostros
    print("Entrenando...")
    face_recognizer.train(facesData, np.array(labels))

    # Almacenando el modelo obtenido
    face_recognizer.write('modeloEigenFace.xml')
    print("Modelo almacenado...")

if __name__ == '__main__':
    dataPath = './resources'  # Ruta de datos
    train_model(dataPath)
