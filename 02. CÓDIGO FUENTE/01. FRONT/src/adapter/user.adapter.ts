import { IUser, UserRequest, UserResponse } from '../types'

export const CreateUserAdapter = (userRespose: UserResponse): IUser => {
  const { userId, name, userName, email, universityId, idRol } = userRespose

  return {
    id: userId,
    nombre: name,
    userName,
    email,
    idUniversitario: universityId,
    idRol,
  } as IUser
}

export const CreateUserListAdapter = (
  userResponseList: UserResponse[],
): IUser[] => {
  return userResponseList.map(CreateUserAdapter)
}

export const CreateUserRequestAdapter = (user: IUser): UserRequest => {
  const { nombre, userName, email, contrasenia, idUniversitario, idRol } = user
  return {
    name: nombre,
    userName,
    email,
    password: contrasenia,
    universityId: idUniversitario,
    idRol,
  } as UserRequest
}
