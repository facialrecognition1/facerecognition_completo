import { ScheduleValidationResponse } from '../types'
import { IScheduleValidation } from '../types/entity/scheduleValidation.entity'

export const createScheduleValidationAdapter = (
  scheduleValidation: IScheduleValidation,
): ScheduleValidationResponse => {
  const { userName, userRole, nrc, message, authorization, startTime } =
    scheduleValidation
  return {
    userName,
    userRole,
    nrc,
    message,
    authorization,
    startTime,
  } as ScheduleValidationResponse
}
