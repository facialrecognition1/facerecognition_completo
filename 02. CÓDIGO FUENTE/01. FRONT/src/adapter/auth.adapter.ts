import { AuthResponse, ICredentials } from '../types'

export const createAuthUserAdapter = (
  credentials: ICredentials,
): AuthResponse => {
  const { userName, password } = credentials

  return { userName, password } as AuthResponse
}
