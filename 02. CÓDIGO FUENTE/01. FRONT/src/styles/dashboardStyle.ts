export const dashboardStyle = {
  width: '70vw',
  height: '80vh',
  padding: '2rem',
  borderRadius: '2rem',
  boxShadow: '0px 10px 15px rgba(0,0,0,0.2)',
}

export const boxCommonStyle = {
  display: 'flex',
  gap: '10px',
  alignItems: 'center',
}
