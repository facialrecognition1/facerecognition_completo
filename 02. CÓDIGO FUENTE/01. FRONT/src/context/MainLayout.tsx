import { Box } from '@mui/material'
import React, { ReactNode } from 'react'

interface MainLayoutProps {
  children: ReactNode
}

const MainLayout: React.FC<MainLayoutProps> = (props) => {
  return <div>{props.children}</div>
}

export default MainLayout
