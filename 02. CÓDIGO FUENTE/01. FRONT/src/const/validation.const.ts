export const VALIDATION_DENIED = {
  position: 'top-end',
  icon: 'error',
  title: "you can't enter class",
  showConfirmButton: false,
  timer: 1500,
}

export const VALIDATION_SUCCESS = {
  position: 'top-end',
  icon: 'success',
  title: 'you can enter class',
  showConfirmButton: false,
  timer: 1500,
}
