export type IUser = {
  id: number
  nombre: string
  email: string
  idUniversitario: string
  contrasenia: string
  userName: string
  idRol: string
}
