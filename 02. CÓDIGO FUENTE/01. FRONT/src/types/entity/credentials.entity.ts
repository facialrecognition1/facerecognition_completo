export type ICredentials = {
    userName:string,
    password: string
}