export type ScheduleValidationResponse = {
  userName: string
  userRole: string
  nrc: string
  startTime: string
  authorization: boolean
  message: string
}
