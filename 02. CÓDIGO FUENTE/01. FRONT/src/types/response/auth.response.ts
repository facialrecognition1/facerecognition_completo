export type AuthResponse = {
  userName: string
  password: string
}
