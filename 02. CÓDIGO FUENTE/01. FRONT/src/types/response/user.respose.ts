export type UserResponse = {
  userId: number
  userName: string
  name: string
  idRol: string
  universityId: string
  email: string
}
