export type UserRequest = {
    userName:string,
    password:string,
    name:string,
    idRol:string,
    universityId:string,
    email:string,

}