import axios, { AxiosError } from 'axios'
import { PATHS } from '../routes'
import { FaceRequest } from '../types/request/face.request'

const facialRecognitionApi = 'http://127.0.0.1:4000'

export const getUserNameWithFaceService = async () => {
  try {
    const response = (
      await axios.get(`${facialRecognitionApi}${PATHS.userRecognition}`)
    ).data
    return response
  } catch (error) {
    throw error as AxiosError
  }
}

export const addUserFacesService = async (faceRequest: FaceRequest) => {
  try {
    const response = await axios.post(
      `${facialRecognitionApi}${PATHS.addFace}`,
      faceRequest,
    )
    return response
  } catch (error) {
    throw error as AxiosError
  }
}
