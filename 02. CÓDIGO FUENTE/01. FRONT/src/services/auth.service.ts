import axios, { AxiosError } from 'axios'
import { CreateUserAdapter } from '../adapter'
import { PATHS } from '../routes'
import { UserResponse } from '../types'

const apiUrl = 'http://localhost:8080/facialRecognition'

export const getAuthenticatedUserService = async (
  userName: string,
  password: string,
) => {
  try {
    const response = (
      await axios.get(`${apiUrl}${PATHS.authUser}`, {
        params: { userName, password },
      })
    ).data as UserResponse
    return CreateUserAdapter(response)
  } catch (error) {
    throw error as AxiosError
  }
}
