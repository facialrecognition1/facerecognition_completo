import axios, { AxiosError } from 'axios'
import { PATHS } from '../routes'
import { IUser, ScheduleValidationResponse, UserResponse } from '../types'
import {
  CreateUserAdapter,
  CreateUserListAdapter,
  CreateUserRequestAdapter,
} from '../adapter'
import { createScheduleValidationAdapter } from '../adapter/scheduleValidation.adapter'

const apiUrl = 'http://localhost:8080/facialRecognition'

const instance = axios.create({
  baseURL: process.env.REST_SERVICE_API,
})

export const getUserByIdService = async (userId: number) => {
  try {
    const response = (
      await instance.get(`${PATHS.user}`, {
        params: { userId },
      })
    ).data as UserResponse
    return CreateUserAdapter(response)
  } catch (error) {
    throw error as AxiosError
  }
}

export const getUserByUserNameOrUniversityIdService = async (
  userName?: String,
  universityId?: String,
) => {
  try {
    const response = (
      await axios.get(`${apiUrl}${PATHS.user}${PATHS.find}`, {
        params: { userName, universityId },
      })
    ).data as UserResponse
    return CreateUserAdapter(response)
  } catch (error) {
    throw error as AxiosError
  }
}

export const getAllUsersService = async () => {
  try {
    const response = (await axios.get(`${apiUrl}${PATHS.user}${PATHS.all}`))
      .data as UserResponse[]
    return CreateUserListAdapter(response)
  } catch (error) {
    throw error as AxiosError
  }
}

export const createUserService = async (user: IUser) => {
  try {
    const request = CreateUserRequestAdapter(user)
    const response = await axios.post(`${apiUrl}${PATHS.user}`, request)
    return response.data
  } catch (error) {
    throw error as AxiosError
  }
}

export const validateScheduleService = async (userName: string) => {
  try {
    const response = (
      await axios.get(`${apiUrl}${PATHS.schedule}${PATHS.validation}`, {
        params: { userName },
      })
    ).data as ScheduleValidationResponse
    return createScheduleValidationAdapter(response)
  } catch (error) {
    throw error as AxiosError
  }
}

export const getNrcListByUserNameService = async (userName: string) => {
  try {
    const response = (
      await axios.get(`${apiUrl}${PATHS.userNrc}${PATHS.all}`, {
        params: { userName },
      })
    ).data
  } catch (error) {
    throw error as AxiosError
  }
}
