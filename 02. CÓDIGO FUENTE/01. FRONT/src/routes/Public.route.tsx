import { RouteObject } from 'react-router-dom'
import { ROUTES } from './path.route'
import { Home, HomePage, Login } from '../pages'
import { boxContainerStyle } from '../styles'
import { Box } from '@mui/material'

export const PublicRoute: RouteObject = {
  path: ROUTES.none,
  children: [
    {
      index: true,
      element: <div>Public Route</div>,
    },
    {
      path: `${ROUTES.home}`,
      element: (
        <Box sx={boxContainerStyle}>
          <Home />
        </Box>
      ),
    },
    {
      path: `homeTest`,
      element: (
        <Box sx={boxContainerStyle}>
          <HomePage />
        </Box>
      ),
    },
    {
      path: `${ROUTES.login}`,
      element: (
        <Box sx={boxContainerStyle}>
          <Login />
        </Box>
      ),
    },
  ],
}
