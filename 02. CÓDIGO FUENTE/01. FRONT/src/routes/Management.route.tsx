import { RouteObject } from 'react-router-dom'
import { AdminAuthGuard } from '../guards'
import { Dashboard } from '../pages'
import { ROUTES } from './path.route'

export const ManagementRoute: RouteObject = {
  path: ROUTES.management,
  element: <AdminAuthGuard />,
  children: [
    {
      path: `${ROUTES.none}`,
      element: <div>Management Route</div>,
    },
    {
      path: `${ROUTES.dashboard}`,
      element: <Dashboard />,
    },
  ],
}
