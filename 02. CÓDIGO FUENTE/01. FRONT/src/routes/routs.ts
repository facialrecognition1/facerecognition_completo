export const PATHS = {
  user: '/user',
  userNrc: '/userNrc',
  userId: '/userId',
  all: '/all',
  find: '/find',
  userRecognition: '/recognize_face',
  addFace: '/capture_images',
  authUser: '/authenticationUser',
  schedule: '/schedule',
  validation: '/validation',
  params: {
    queryParam: '?',
    userName: 'userName',
  },
}
