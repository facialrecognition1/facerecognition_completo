import { useRoutes } from 'react-router-dom'
import { ManagementRoute, PublicRoute, UserRoute } from '.'

export const Routes = () => useRoutes([PublicRoute, ManagementRoute, UserRoute])
