export const ROUTES = {
  root: '/',
  none: '',
  management: 'management',
  public: 'public',
  home: 'home',
  login: 'login',
  dashboard: 'dashboard',
  user: 'user',
}
