import { RouteObject } from 'react-router'
import { ROUTES } from '.'
import { UserAuthGuard } from '../guards'
import { UserDashboard } from '../pages'
import { boxContainerStyle } from '../styles'
import { Box } from '@mui/material'

export const UserRoute: RouteObject = {
  path: ROUTES.user,
  element: <UserAuthGuard />,
  children: [
    {
      path: `${ROUTES.dashboard}`,
      element: (
        <Box sx={boxContainerStyle}>
          <UserDashboard />
        </Box>
      ),
    },
  ],
}
