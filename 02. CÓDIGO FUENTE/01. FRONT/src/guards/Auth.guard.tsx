import { useSelector } from 'react-redux'
import { getUserSelector } from '../redux'
import { Navigate, Outlet } from 'react-router'
import { ROUTES } from '../routes'
import { ROL } from './enum'

export const AdminAuthGuard = () => {
  const user = useSelector(getUserSelector)

  return user.userState.idRol === ROL.ADMIN ? (
    <Outlet />
  ) : (
    <Navigate to={`/${ROUTES.login}`} />
  )
}
