import { useSelector } from 'react-redux'
import { Navigate, Outlet } from 'react-router'
import { getUserSelector } from '../redux'
import { ROUTES } from '../routes'
import { ROL } from './enum'

export const UserAuthGuard = () => {
  const user = useSelector(getUserSelector)

  return user.userState.idRol === ROL.STUDENT ? (
    <Outlet />
  ) : (
    <Navigate replace to={`/${ROUTES.login}`} />
  )
}
