import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { persistReducer, persistStore } from 'redux-persist'
import storage from 'redux-persist/es/storage'
import thunk from 'redux-thunk'
import { IUser } from '../types'
import { userReducer } from './states'

export interface AppStore {
  user: IUser
}

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['userState'],
}

const rootReducer = combineReducers({
  userState: userReducer,
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
  reducer: {
    user: persistedReducer,
  },
  middleware: [thunk],
})

export type RootState = ReturnType<typeof store.getState>
export const persistor = persistStore(store)
