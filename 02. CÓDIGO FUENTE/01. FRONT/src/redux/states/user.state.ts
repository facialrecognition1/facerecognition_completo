import { createSlice } from '@reduxjs/toolkit'
import { IUser } from '../../types'
import { RootState } from '../store'

export const initialState: IUser = {
  id: 0,
  nombre: '',
  userName: '',
  email: '',
  contrasenia: '',
  idUniversitario: '',
  idRol: '',
}

export const userSlice = createSlice({
  name: 'user',
  initialState: initialState,
  reducers: {
    createUserSelector: (state, action) => action.payload,
    updateUserSelector: (state, action) => {
      return {
        ...state,
        ...action.payload,
      }
    },
    resetUserSelector: () => initialState,
  },
})

export const userReducer = userSlice.reducer
export const getUserSelector = (state: RootState) => state.user
export const { createUserSelector, updateUserSelector, resetUserSelector } =
  userSlice.actions

export default userSlice.reducer
