import Swal, { SweetAlertOptions } from 'sweetalert2'
import { VALIDATION_DENIED, VALIDATION_SUCCESS } from '../const'
import { IScheduleValidation } from '../types'

export const scheduleValidationUtil = (
  scheduleValidation: IScheduleValidation,
) => {
  if (!scheduleValidation.authorization) {
    Swal.fire(VALIDATION_DENIED as SweetAlertOptions)
  } else {
    Swal.fire(VALIDATION_SUCCESS as SweetAlertOptions)
  }
}
