export const loginContainerStyle = {
  margin: '10px',
  padding: '30px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'column',
  gap: '20px',
  maxWidth: '30vw',
  maxHeight: '50vh',
  border: '1px solid rgba(166, 165, 165, 0.325)',
  borderRadius: '25px',
  boxShadow: '4px 4px 30px rgba(125, 122, 122, 0.5)',
}
