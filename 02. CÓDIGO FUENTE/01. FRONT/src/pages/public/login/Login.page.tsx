import React from 'react'
import { Box, Typography, TextField, Button } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router'
import { ROL } from '../../../guards'
import {
  getUserSelector,
  resetUserSelector,
  updateUserSelector,
} from '../../../redux'
import { ROUTES } from '../../../routes'
import { getAuthenticatedUserService } from '../../../services'
import { ICredentials } from '../../../types'
import { INITIAL_CREDENTIALS } from './const'
import { loginContainerStyle } from './styles'

export const Login = () => {
  const dispatch = useDispatch()
  const user = useSelector(getUserSelector)
  const navigate = useNavigate()

  const [credentials, setCredentials] =
    React.useState<ICredentials>(INITIAL_CREDENTIALS)

  const handleCredentials = (value: string, credential: string) => {
    setCredentials({ ...credentials, [credential]: value })
  }

  const getAuthenticatedUser = async (userCredentials: ICredentials) => {
    try {
      const response = await getAuthenticatedUserService(
        userCredentials.userName,
        userCredentials.password,
      )
      dispatch(updateUserSelector(response))
      handleUserRol(user.userState.idRol)
    } catch (error) {
      dispatch(resetUserSelector())
      console.log('error getting data ', error)
    }
  }

  const handleUserRol = (rol: string) => {
    if (rol === ROL.ADMIN)
      navigate(ROUTES.root + ROUTES.management + ROUTES.root + ROUTES.dashboard)
    if (rol === ROL.STUDENT)
      navigate(ROUTES.root + ROUTES.user + ROUTES.root + ROUTES.dashboard)
  }

  const handleLogin = () => {
    getAuthenticatedUser(credentials)
  }

  return (
    <Box sx={loginContainerStyle}>
      <Typography variant="h3">Login</Typography>
      <TextField
        label="User Name"
        value={credentials.userName}
        onChange={(event: { target: { value: string } }) =>
          handleCredentials(event.target.value, 'userName')
        }
      />
      <TextField
        label="Password"
        type="password"
        value={credentials.password}
        onChange={(event: { target: { value: string } }) =>
          handleCredentials(event.target.value, 'password')
        }
      />

      <Button onClick={() => handleLogin()}>Login</Button>
    </Box>
  )
}
