import { Box, Button, Icon, TextField, Typography } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router'
import { getUserSelector, resetUserSelector } from '../../../../redux'
import { ROUTES } from '../../../../routes'
import { boxCommonStyle, dashboardStyle } from '../../../../styles'
import { useState } from 'react'

export const UserDashboard = () => {
  const [nrc, setNrc] = useState<Number>()
  const user = useSelector(getUserSelector)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleLogout = () => {
    dispatch(resetUserSelector())
    navigate(ROUTES.root + ROUTES.login)
  }

  const handleNrc = (value: string) => {
    setNrc(Number(value))
  }

  const addNrcToUser = () => {
    return console.log('nrc', nrc)
  }

  return (
    <Box sx={dashboardStyle}>
      <Box sx={{ height: '97%' }}>
        <Typography align="center" variant="h3">
          {user.userState.userName.toUpperCase()}
        </Typography>
        <Box sx={boxCommonStyle}>
          <TextField
            label="NRC"
            variant="outlined"
            value={nrc}
            onChange={(e) => handleNrc(e.target.value)}
          />
          <Button onClick={() => addNrcToUser()}>Add NRC</Button>
        </Box>
      </Box>
      <Button onClick={() => handleLogout()}>LogOut</Button>
    </Box>
  )
}
