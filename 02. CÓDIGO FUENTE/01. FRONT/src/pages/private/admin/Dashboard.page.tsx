import { Box, Button } from '@mui/material'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router'
import { getUserSelector, resetUserSelector } from '../../../redux'
import { ROUTES } from '../../../routes'
import { useSelector } from 'react-redux'
import { AddNrc } from '.'

export const Dashboard = () => {
  const user = useSelector(getUserSelector)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleLogout = () => {
    dispatch(resetUserSelector())
    navigate(ROUTES.root + ROUTES.login)
  }

  return (
    <Box>
      <AddNrc />
      <Button
        onClick={() => {
          handleLogout()
        }}
      >
        LogOut
      </Button>
    </Box>
  )
}
