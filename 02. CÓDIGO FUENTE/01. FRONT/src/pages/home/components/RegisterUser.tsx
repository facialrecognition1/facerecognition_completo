import {
  Box,
  Button,
  Dialog,
  DialogContent,
  TextField,
  Typography,
  Zoom,
} from '@mui/material'
import { isAxiosError } from 'axios'
import { useEffect, useState } from 'react'
import { addUserFacesService, createUserService } from '../../../services'
import { IUser } from '../../../types'
import { INITIAL_USER } from '../const'
import { registerUserContainerStyle } from '../styles'
import { useDispatch } from 'react-redux'
import { resetUserSelector } from '../../../redux'

export const RegisterUser = () => {
  const dispatch = useDispatch()
  const [open, setOpen] = useState<boolean>(false)
  const [userRequest, setUserRequest] = useState<IUser>(INITIAL_USER)
  const [isFormComplete, setIsFormComplete] = useState<boolean>(false)

  const handleChangeUserData = (key: keyof IUser, value: string) => {
    setUserRequest({ ...userRequest, [key]: value })

    const allFieldsComplete = Object.values(userRequest).every(
      (value) => !!value,
    )
    setIsFormComplete(allFieldsComplete)
  }

  const handleCreateUser = async () => {
    try {
      const createdUser = await createUserService(userRequest)
      addUserFaces(userRequest.userName)
    } catch (error) {
      if (isAxiosError(error)) console.log(error.response?.data.message)
    }
  }

  const addUserFaces = async (name: string) => {
    const faceData = {
      personName: name,
      dataPath: './resources',
    }
    try {
      const response = await addUserFacesService(faceData)
      console.log(response)
      handleCreateUser()
    } catch (error) {
      if (isAxiosError(error)) console.log(error.response?.data.message)
    }
  }

  useEffect(() => {
    dispatch(resetUserSelector)
  }, [])

  return (
    <Box>
      <Button
        onClick={() => setOpen(!open)}
        style={{ width: '100%', marginTop: '20px', color: '#fff' }}
      >
        Crear Usuario
      </Button>
      <Dialog
        open={open}
        onClose={() => setOpen(!open)}
        TransitionComponent={Zoom}
        transitionDuration={500}
      >
        <DialogContent sx={registerUserContainerStyle}>
          <Typography
            variant="h5"
            style={{ marginBottom: '20px', fontWeight: 'bold' }}
          >
            Registro nuevo usuario
          </Typography>
          <TextField
            label="Nombre"
            variant="outlined"
            fullWidth
            required
            value={userRequest.nombre}
            onChange={(e) => handleChangeUserData('nombre', e.target.value)}
          />
          <TextField
            label="Correo Electrónico"
            variant="outlined"
            fullWidth
            required
            value={userRequest.email}
            onChange={(e) => handleChangeUserData('email', e.target.value)}
          />
          <TextField
            label="ID Universitario"
            variant="outlined"
            fullWidth
            required
            value={userRequest.idUniversitario}
            onChange={(e) =>
              handleChangeUserData('idUniversitario', e.target.value)
            }
          />
          <TextField
            label="Nombre de Usuario"
            variant="outlined"
            fullWidth
            required
            value={userRequest.userName}
            onChange={(e) => handleChangeUserData('userName', e.target.value)}
          />
          <TextField
            label="Contraseña"
            variant="outlined"
            fullWidth
            required
            type="password"
            value={userRequest.contrasenia}
            onChange={(e) =>
              handleChangeUserData('contrasenia', e.target.value)
            }
          />
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-around',
              gap: '10px',
            }}
          >
            <Button
              variant="outlined"
              onClick={handleCreateUser}
              disabled={!isFormComplete}
            >
              Registrar rostros y finalizar
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
    </Box>
  )
}
