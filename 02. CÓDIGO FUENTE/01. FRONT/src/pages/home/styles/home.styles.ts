export const homeContainerStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  minHeight: '100vh',
}

export const bodyStyle = {
  backgroundSize: 'cover',
  filter: 'blur(4px)',
  position: 'absolute',
  width: '100%',
  height: '100%',
  zIndex: -1,
}

export const formStyle = {
  background: 'rgba(5,3,33,255)',
  flex: '0 0 30%',
  borderRadius: '8px',
  padding: '20px',
  color: 'white',
  textAlign: 'center',
}

export const registerUserContainerStyle = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  gap: '25px',
  width: '450px',
}

export const registerInputs = {
  width: '100vw',
}
