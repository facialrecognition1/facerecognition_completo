import { IUser } from '../../../types'

export const INITIAL_USER = {
  userName: '',
  contrasenia: '',
  nombre: '',
  idRol: 'S',
  email: '',
  idUniversitario: '',
} as IUser
