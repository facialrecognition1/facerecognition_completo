import { Box, Container, Typography } from '@mui/material'
import image1 from '../../assets/espe.jpg'
import image3 from '../../assets/espe2.jpg'
import image2 from '../../assets/espe4.jpg'
import image4 from '../../assets/espe5.jpg'
import miGif from '../../assets/facial.gif'
import { RegisterUser } from './components'
import { bodyStyle, formStyle, homeContainerStyle } from './styles'
import { useEffect, useState } from 'react'
import { IScheduleValidation, IUser } from '../../types'
import {
  getUserNameWithFaceService,
  validateScheduleService,
} from '../../services'
import { scheduleValidationUtil } from '../../util/scheduleValidation.util'

export const HomePage = () => {
  const imagePaths = [image1, image2, image3, image4]
  const [user, setUser] = useState<string>('')
  const [scheduleValidate, setScheduleValidate] =
    useState<IScheduleValidation>()

  const getUserNameWithFace = async () => {
    try {
      const response = await getUserNameWithFaceService()
      setUser(response.recognized_names.at(0))
      console.log('user', user)
      if (user) scheduleValidation(user)
    } catch (error) {
      console.log('error getting data')
    }
  }

  const scheduleValidation = async (userName: string) => {
    try {
      const response = await validateScheduleService(userName)
      console.log('response', response)
      if (Boolean(response)) {
        setScheduleValidate(response)
        console.log('scheduleValidate', scheduleValidate)
        scheduleValidationUtil(scheduleValidate!)
      }
    } catch (error) {
      console.log('Error al obtener datos', error)
    }
  }

  setTimeout(() => {
    getUserNameWithFace()
  }, 2000)

  useEffect(() => {
    getUserNameWithFace()
  }, [] )

  return (
    <Container maxWidth="lg" style={homeContainerStyle}>
      <Box sx={{ ...bodyStyle, backgroundImage: `url(${imagePaths[0]})` }} />
      <Container sx={{ ...formStyle, filter: 'none' }}>
        <Typography
          variant="h5"
          style={{ marginBottom: '20px', fontWeight: 'bold' }}
        >
          Bienvenido a Facial Recognition ESPE
        </Typography>
        <img
          src={miGif}
          alt="Descripción del GIF"
          style={{ width: '70%', filter: 'none' }}
        />
        <RegisterUser />
      </Container>
    </Container>
  )
}
