import { useEffect, useState } from 'react'
import image1 from '../../assets/espe.jpg'
import image3 from '../../assets/espe2.jpg'
import image2 from '../../assets/espe4.jpg'
import image4 from '../../assets/espe5.jpg'
import miGif from '../../assets/facial.gif'
import {
  addUserFacesService,
  createUserService,
  getUserNameWithFaceService,
  validateScheduleService,
} from '../../services'
import { IScheduleValidation, IUser } from '../../types'
import { INITIAL_USER } from './const/initalUser.const'
import {
  Box,
  Button,
  Container,
  Dialog,
  DialogContent,
  TextField,
  Typography,
} from '@mui/material'
import { bodyStyle, formStyle, homeContainerStyle } from './styles'
import { scheduleValidationUtil } from '../../util/scheduleValidation.util'

const formDataInitialState = {
  nombre: '',
  email: '',
  id_universitario: '',
  contraseña: '',
  user_name: '',
}

export const Home = () => {
  const imagePaths = [image1, image2, image3, image4]
  const [imageIndex, setImageIndex] = useState<number>(0)
  const [open, setOpen] = useState<boolean>(false)
  const [registroCompleto, setRegistroCompleto] = useState<boolean>(false)
  const [userRequest, setUserRequest] = useState<IUser>(INITIAL_USER)
  const [formData, setFormData] = useState(formDataInitialState)
  const [isLoading, setIsLoading] = useState(false)
  const [user, setUser] = useState<string>('desconocido')
  const [scheduleValidate, setScheduleValidate] =
    useState<IScheduleValidation>()

  const handleCreateUser = async () => {
    try {
      const createdUser = await createUserService(userRequest)
      console.log('Usuario creado:', createdUser)
      setRegistroCompleto(true)
      setTimeout(() => {
        setRegistroCompleto(false)
      }, 3000)
      handleModal()
    } catch (error) {
      console.error('Error al crear usuario:', error)
    }
  }

  const handleImage = () => {
    const interval = setInterval(() => {
      setImageIndex((prevIndex) => (prevIndex + 1) % imagePaths.length)
    }, 10000)

    return () => {
      clearInterval(interval)
    }
  }

  const getUserNameWithFace = async () => {
    try {
      const response = await getUserNameWithFaceService()
      setUser(response.recognized_names.at(0))
      console.log('getUser', user)
      scheduleValidation(user)
    } catch (error) {
      console.log('erroor getting data')
    }
  }

  const addUserFaces = async (nombre: string) => {
    setIsLoading(true)
    const faceData = {
      personName: nombre,
      dataPath: './resources',
    }
    try {
      const response = await addUserFacesService(faceData)
      console.log(response)
      handleCreateUser()
    } catch (error) {
      console.log('Error al obtener datos', error)
    } finally {
      setIsLoading(false)
    }
  }

  const scheduleValidation = async (userName: string) => {
    try {
      const response = await validateScheduleService(userName)
      console.log('scheduleValidation', response)
      setUser(response.userName)
      setScheduleValidate(response)
      scheduleValidationUtil(scheduleValidate!)
    } catch (error) {
      console.log('Error al obtener datos', error)
      setUser('')
    }
  }

  const handleModal = () => {
    setOpen(!open)
  }

  const isFormValid = () => {
    return (
      userRequest.nombre.trim() !== '' &&
      userRequest.email.trim() !== '' &&
      userRequest.idUniversitario.trim() !== '' &&
      userRequest.contrasenia.trim() !== '' &&
      userRequest.userName.trim() !== ''
    )
  }

  const handleSubmit = () => {
    if (isFormValid()) {
      setFormData(formDataInitialState)
      handleCreateUser()
    } else {
      alert(
        'Por favor, complete todos los campos antes de enviar el formulario.',
      )
    }
  }

  useEffect(() => {
    getUserNameWithFace()
  }, [user])

  return (
    <Container maxWidth="lg" style={homeContainerStyle}>
      <Box
        sx={{ ...bodyStyle, backgroundImage: `url(${imagePaths[imageIndex]})` }}
      />
      <Box sx={formStyle}>
        <Container style={{ filter: 'none' }}>
          <Typography
            variant="h5"
            style={{ marginBottom: '20px', fontWeight: 'bold' }}
          >
            Bienvenido a Facial Recognition ESPE
          </Typography>
          <img
            src={miGif}
            alt="Descripción del GIF"
            style={{ width: '70%', filter: 'none' }}
          />
          <div style={{ marginTop: '20px' }}>
            <Button
              onClick={handleModal}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: '100%', filter: 'none' }}
            >
              Crear Usuario
            </Button>
          </div>
        </Container>
        <Dialog open={open} onClose={handleModal}>
          <DialogContent>
            <Typography
              variant="h5"
              style={{ marginBottom: '20px', fontWeight: 'bold' }}
            >
              Registro nuevo usuario
            </Typography>
            <form>
              <TextField
                label="Nombre"
                variant="outlined"
                fullWidth
                margin="normal"
                required
                name="name"
                value={userRequest.nombre}
                onChange={(e) =>
                  setUserRequest((prevUserRequest) => ({
                    ...prevUserRequest,
                    nombre: e.target.value,
                  }))
                }
              />
              <TextField
                label="Correo Electrónico"
                variant="outlined"
                fullWidth
                margin="normal"
                required
                name="email"
                value={userRequest.email}
                onChange={(e) =>
                  setUserRequest((prevUserRequest) => ({
                    ...prevUserRequest,
                    email: e.target.value,
                  }))
                }
              />
              <TextField
                label="ID Universitario"
                variant="outlined"
                fullWidth
                margin="normal"
                required
                name="universityId"
                value={userRequest.idUniversitario}
                onChange={(e) =>
                  setUserRequest((prevUserRequest) => ({
                    ...prevUserRequest,
                    idUniversitario: e.target.value,
                  }))
                }
              />
              <TextField
                label="Contraseña"
                variant="outlined"
                fullWidth
                margin="normal"
                required
                name="password"
                type="password"
                value={userRequest.contrasenia}
                onChange={(e) =>
                  setUserRequest((prevUserRequest) => ({
                    ...prevUserRequest,
                    contrasenia: e.target.value,
                  }))
                }
              />
              <TextField
                label="Nombre de Usuario"
                variant="outlined"
                fullWidth
                margin="normal"
                required
                name="userName"
                value={userRequest.userName}
                onChange={(e) =>
                  setUserRequest((prevUserRequest) => ({
                    ...prevUserRequest,
                    userName: e.target.value,
                  }))
                }
              />
              <Button
                type="button"
                variant="contained"
                fullWidth
                color="primary"
                onClick={() => addUserFaces(userRequest.userName)}
                sx={{ mt: 2 }}
              >
                Registrar rostros
              </Button>
              <Button
                type="button"
                variant="contained"
                fullWidth
                color="primary"
                onClick={handleCreateUser}
                disabled={!isFormValid()}
                sx={{ mt: 2 }}
              >
                Finalizar Registro
              </Button>
            </form>
          </DialogContent>
        </Dialog>
        {registroCompleto && (
          <Dialog
            open={registroCompleto}
            onClose={() => setRegistroCompleto(false)}
          >
            <DialogContent>
              <Typography variant="h6" gutterBottom>
                Registro completado. ¡Bienvenido!
              </Typography>
            </DialogContent>
          </Dialog>
        )}
      </Box>
      <Dialog open={isLoading} onClose={() => setIsLoading(false)}>
        <DialogContent>
          <Typography variant="h6" gutterBottom>
            Espere un momento, capturando rostro...
          </Typography>
        </DialogContent>
      </Dialog>
    </Container>
  )
}
