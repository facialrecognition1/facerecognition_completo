package com.facialRecognition.facialRecognition_rest_ws.request;

public class UserNrcRequest {
    public Long userId;
    public Long nrcId;

    public UserNrcRequest(Long userId, Long nrcId) {
        this.userId = userId;
        this.nrcId = nrcId;
    }

    public UserNrcRequest() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getNrcId() {
        return nrcId;
    }

    public void setNrcId(Long nrcId) {
        this.nrcId = nrcId;
    }
}
