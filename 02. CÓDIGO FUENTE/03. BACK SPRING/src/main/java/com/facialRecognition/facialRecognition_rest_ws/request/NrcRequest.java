package com.facialRecognition.facialRecognition_rest_ws.request;

public class NrcRequest {

    public String description;
    public String course;
    public Long scheduleId;
    public String nrcCode;

    public NrcRequest() {
    }

    public NrcRequest(String description, String course, Long scheduleId, String nrcCode) {
        this.description = description;
        this.course = course;
        this.scheduleId = scheduleId;
        this.nrcCode = nrcCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getNrcCode() {
        return nrcCode;
    }

    public void setNrcCode(String nrcCode) {
        this.nrcCode = nrcCode;
    }


}
