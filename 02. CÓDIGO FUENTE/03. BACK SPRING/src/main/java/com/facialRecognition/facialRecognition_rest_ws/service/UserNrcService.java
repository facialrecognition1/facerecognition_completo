package com.facialRecognition.facialRecognition_rest_ws.service;

import com.facialRecognition.facialRecognition_rest_ws.entity.Nrc;
import com.facialRecognition.facialRecognition_rest_ws.entity.User;
import com.facialRecognition.facialRecognition_rest_ws.entity.UserNrc;
import com.facialRecognition.facialRecognition_rest_ws.repository.NrcRepository;
import com.facialRecognition.facialRecognition_rest_ws.repository.UserNrcRepository;
import com.facialRecognition.facialRecognition_rest_ws.repository.UserRepository;
import com.facialRecognition.facialRecognition_rest_ws.request.UserNrcRequest;
import com.facialRecognition.facialRecognition_rest_ws.response.IUserNrcResponse;
import com.facialRecognition.facialRecognition_rest_ws.response.IUserResponse;
import com.facialRecognition.facialRecognition_rest_ws.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class UserNrcService {

    @Autowired
    UserNrcRepository userNrcRepository;
    @Autowired
    NrcRepository nrcRepository;
    @Autowired
    UserRepository userRepository;
    public IUserNrcResponse getUserNrc(Long userNrcId){
        IUserNrcResponse userNrc = userNrcRepository.findByUserNrcId(userNrcId);
        if(userNrc == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NRC_NOT_FOUND);

        return userNrc;
    }

    public List<IUserNrcResponse> getAllUserNrcs(String userName){
        User user = userRepository.findByUserName(userName);

        if(user == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NRC_NOT_FOUND);

        List<IUserNrcResponse> userNrcs = userNrcRepository.findAllNrcByUserName(userName);
        return userNrcs;
    }

    public IUserNrcResponse addNrcToUser(UserNrcRequest userRequest){
        UserNrc userNrc = new UserNrc();
        User user = userRepository.findById(userRequest.getUserId()).orElse(null);
        Nrc nrc = nrcRepository.findById(userRequest.nrcId).orElse(null);

        if(user == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND);
        if(nrc == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.NRC_NOT_FOUND);

        userNrc.setNrc(nrc);
        userNrc.setUser(user);
        UserNrc userNrcSave = userNrcRepository.save(userNrc);

        IUserNrcResponse userNrcResponse = userNrcRepository.findByUserNrcId(userNrcSave.getUserNrcId());
        return userNrcResponse;

    }

    public IUserNrcResponse updateUserNrcByUserNrcId(Long userNrcId, UserNrcRequest userNrcRequest){
        UserNrc userNrc = userNrcRepository.findById(userNrcId).orElse(null);
        Nrc nrc = nrcRepository.findById(userNrcRequest.getNrcId()).orElse(null);

        if(userNrc == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NRC_NOT_FOUND);
        if(nrc == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.NRC_NOT_FOUND);

        userNrc.setNrc(nrc);
        UserNrc userNrcSave = userNrcRepository.save(userNrc);

        IUserNrcResponse userNrcResponse = userNrcRepository.findByUserNrcId(userNrcSave.getUserNrcId());
        return userNrcResponse;
    }

    public List<IUserNrcResponse> deleteUserNrcById(Long userNrcId, Long userId) {
        UserNrc userNrc = userNrcRepository.findById(userNrcId).orElse(null);
        User user = userRepository.findById(userId).orElse(null);

        if (userNrc == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NRC_NOT_FOUND);
        if (user == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND);

        userNrcRepository.delete(userNrc);
        List<IUserNrcResponse> getAllUserNrcs = userNrcRepository.findAllNrcByUserName(user.getUserName());
        return getAllUserNrcs;
    }
}
