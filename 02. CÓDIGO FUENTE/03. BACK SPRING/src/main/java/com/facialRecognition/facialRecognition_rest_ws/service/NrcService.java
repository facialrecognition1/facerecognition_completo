package com.facialRecognition.facialRecognition_rest_ws.service;

import com.facialRecognition.facialRecognition_rest_ws.entity.Nrc;
import com.facialRecognition.facialRecognition_rest_ws.entity.User;
import com.facialRecognition.facialRecognition_rest_ws.repository.NrcRepository;
import com.facialRecognition.facialRecognition_rest_ws.repository.UserRepository;
import com.facialRecognition.facialRecognition_rest_ws.request.NrcRequest;
import com.facialRecognition.facialRecognition_rest_ws.response.INrcResponse;
import com.facialRecognition.facialRecognition_rest_ws.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class NrcService {

    @Autowired
    NrcRepository nrcRepository;

    @Autowired
    UserRepository userRepository;

    public Boolean validateCourse(List<INrcResponse> nrcResponse, String course){
        for (INrcResponse nrc : nrcResponse){
            if(nrc.getCurse().equals(course)) return false;
        }
        return  true;
    }

    public INrcResponse createNRC(Long userId, NrcRequest nrcRequest){
        Nrc nrc = new Nrc();

        User user = userRepository.findById(userId).orElse(null);
        INrcResponse nrcCode = nrcRepository.getNrcByNrcCode(nrcRequest.getNrcCode());
        List<INrcResponse> listNrcByScheduleId = getAllNrcsByScheduleId(nrcRequest.getScheduleId());
        String course = nrcRequest.getCourse();

        if(user == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND);
        if(nrcCode != null) throw new ResponseStatusException(HttpStatus.MULTI_STATUS, Message.NRC_ALREADY_EXIST);
        if(!validateCourse(listNrcByScheduleId, course)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Message.NRC_COURSE_ALREADY_EXIST);

        nrc.setDescription(nrcRequest.getDescription());
        nrc.setNrcCode(nrcRequest.getNrcCode());
        nrc.setScheduleId(nrcRequest.getScheduleId());
        nrc.setCurse(nrcRequest.getCourse());

        Nrc nrcSave = nrcRepository.save(nrc);
        INrcResponse nrcResponse = nrcRepository.findByNrcId(nrc.getNrcId());
        return nrcResponse;
    }

    public INrcResponse getNrcByNrcCode(String nrcCode){
        INrcResponse nrc = nrcRepository.getNrcByNrcCode(nrcCode);
        if (nrc == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.NRC_NOT_FOUND);
        return nrc;
    }

    public List<INrcResponse> getAllNrc(){
        List<INrcResponse> nrc = nrcRepository.getAllNrcs();
        if(nrc == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.NRC_NOT_FOUND);
        return nrc;
    }

    public List<INrcResponse> getAllNrcsByScheduleId(Long scheduleId) {
                List<INrcResponse> nrcs = nrcRepository.getAllNrcsByScheduleId(scheduleId);
        return nrcs;
    }

    public INrcResponse updateNrc(String nrcCode, NrcRequest nrcRequest){
        Nrc nrc = nrcRepository.findByNrcCode(nrcCode);
        Nrc nrcCodeRequest = nrcRepository.findByNrcCode(nrcRequest.getNrcCode());

        if(nrc == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.NRC_NOT_FOUND);
        if(nrcCodeRequest != null) throw new ResponseStatusException(HttpStatus.MULTI_STATUS, Message.NRC_ALREADY_EXIST);

        nrc.setDescription(nrcRequest.getDescription());
        nrc.setCurse(nrcRequest.getCourse());
        nrc.setScheduleId(nrcRequest.getScheduleId());
        nrc.setNrcCode(nrcRequest.getNrcCode());

        Nrc nrcSave = nrcRepository.save(nrc);
        INrcResponse nrcResponse = nrcRepository.getNrcByNrcCode(nrc.getNrcCode());
        return  nrcResponse;
    }
}
