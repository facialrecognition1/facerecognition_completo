package com.facialRecognition.facialRecognition_rest_ws.util;

public class Message {
    public static final String USER_NOT_FOUND = "user not found";

    //users
    public static final String USERNAME_ALREADY_EXIST = "UserName already exist";
    public static final String EMAIL_ALREADY_EXIST = "Email already exist";
    public static final String UNIVERSITY_ID_ALREADY_EXIST = "University id already exist";
    public static final String INVALID_EMAIL = "Email is invalid";

    //NRC
    public static final String NRC_NOT_FOUND = "Nrc not found";
    public static final String NRC_ALREADY_EXIST = "Nrc already exist";
    public static final String NRC_COURSE_ALREADY_EXIST = "This course has already been assigned";

    //USER_NRC
    public static final String USER_NRC_NOT_FOUND = "User Nrc not found";

    //AUTH
    public static final String AUTH_DECLINE = "user or password incorrect";
}
