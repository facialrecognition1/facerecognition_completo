package com.facialRecognition.facialRecognition_rest_ws.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface IUserResponse {

    @JsonProperty("userId")
    Long getUserId();

    @JsonProperty("name")
    String getName();

    @JsonProperty("email")
    String getEmail();

    @JsonProperty("universityId")
    String getUniversityId();

    @JsonProperty("password")
    String getPassword();

    @JsonProperty("userName")
    String getUserName();

    @JsonProperty("idRol")
    String getIdRol();


}
