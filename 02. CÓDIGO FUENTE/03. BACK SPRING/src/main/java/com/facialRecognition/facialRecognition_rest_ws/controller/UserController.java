package com.facialRecognition.facialRecognition_rest_ws.controller;

import com.facialRecognition.facialRecognition_rest_ws.request.UserRequest;
import com.facialRecognition.facialRecognition_rest_ws.service.UserService;
import com.facialRecognition.facialRecognition_rest_ws.util.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(Path.USER)
public class UserController {

    @Autowired
    private UserService userService;
    @GetMapping
    public ResponseEntity getUserByUserId(@RequestParam("userId") Long userId) {
        return new ResponseEntity(userService.getUserById(userId), HttpStatus.OK);
    }

    @GetMapping(value = Path.FIND_BY_USERNAME_UNIVERSITY_ID)
    public ResponseEntity getUserByUserNameOrUniversityId(
            @RequestParam(value = "userName", required = false) String userName,
            @RequestParam(value = "universityId", required = false) String universityId) {
        return new ResponseEntity(userService.getUserByUserNameOrUniversityId(userName, universityId), HttpStatus.OK);
    }

    @GetMapping(value = Path.ALL)
    public ResponseEntity getAllUsers() {
        return new ResponseEntity(userService.getAllUsers(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity createNewUser(@RequestBody UserRequest userRequest){
        return new ResponseEntity(userService.createNewUser(userRequest), HttpStatus.OK);
    }

    @PutMapping(value = Path.ID)
    public ResponseEntity editUser(@PathVariable Long id, @RequestBody UserRequest userRequest){
        return new ResponseEntity(userService.editUser(id, userRequest), HttpStatus.OK);
    }

    @DeleteMapping(value = Path.ID)
    public ResponseEntity editUser(@PathVariable Long id){
        return new ResponseEntity(userService.deleteUserByUserId(id), HttpStatus.OK);
    }
}
