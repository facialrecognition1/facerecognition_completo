package com.facialRecognition.facialRecognition_rest_ws.repository;

import com.facialRecognition.facialRecognition_rest_ws.entity.UserNrc;
import com.facialRecognition.facialRecognition_rest_ws.response.IUserNrcResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserNrcRepository extends CrudRepository<UserNrc, Long> {

    IUserNrcResponse findByUserName(String userName);

    @Query("SELECT n FROM UserNrc n WHERE n.user.userName = :userName")
    List<IUserNrcResponse> findAllNrcByUserName(String userName);
    IUserNrcResponse findByUserNrcId(Long userNrcId);
}
