package com.facialRecognition.facialRecognition_rest_ws.response;

import com.facialRecognition.facialRecognition_rest_ws.entity.Nrc;
import com.facialRecognition.facialRecognition_rest_ws.entity.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Value;

public interface IUserNrcResponse {

    @JsonProperty("userNrcId")
    Long getUserNrcId();

    @Value("#{target.nrc.nrcCode}")
    String getNrc();

    @Value("#{target.user.userName}")
    String getuserName();
}
