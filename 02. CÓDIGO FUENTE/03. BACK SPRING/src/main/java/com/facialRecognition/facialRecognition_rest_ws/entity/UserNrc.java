package com.facialRecognition.facialRecognition_rest_ws.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "user_nrc")
public class UserNrc {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user_nrc")
    private Long userNrcId;

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private User user;

    @ManyToOne
    @JoinColumn(name = "id_NRC")
    private Nrc nrc;

    public UserNrc() {
    }

    public UserNrc(Long userNrcId, User user, Nrc nrc) {
        this.userNrcId = userNrcId;
        this.user = user;
        this.nrc = nrc;
    }

    public Long getUserNrcId() {
        return userNrcId;
    }

    public void setUserNrcId(Long userNrcId) {
        this.userNrcId = userNrcId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Nrc getNrc() {
        return nrc;
    }

    public void setNrc(Nrc nrc) {
        this.nrc = nrc;
    }
}
