package com.facialRecognition.facialRecognition_rest_ws.repository;

import com.facialRecognition.facialRecognition_rest_ws.entity.User;
import com.facialRecognition.facialRecognition_rest_ws.response.IUserResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    IUserResponse getUserByUserId(long userId);
    User findByUserName(String userName);
    User findByEmail(String userEmail);
    User findByUniversityId(String universityId);

    IUserResponse findByUserNameOrUniversityId(String userName, String universityId);

    @Query("SELECT u FROM User u")
    List<IUserResponse> getAllUsers();


}
