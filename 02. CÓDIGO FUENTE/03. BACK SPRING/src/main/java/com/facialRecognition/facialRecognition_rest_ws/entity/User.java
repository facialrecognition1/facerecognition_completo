package com.facialRecognition.facialRecognition_rest_ws.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "usuarios")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Long userId;

    @Column(name = "nombre")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "id_universitario")
    private String universityId;

    @Column(name = "contrasenia")
    private String password;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "idRol")
    private String idRol;

    public User() {
    }

    public User(Long userId, String name, String email, String universityId, String password, String userName, String idRol) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.universityId = universityId;
        this.password = password;
        this.userName = userName;
        this.idRol = idRol;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUniversityId() {
        return universityId;
    }

    public void setUniversityId(String universityId) {
        this.universityId = universityId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIdRol() {
        return idRol;
    }

    public void setIdRol(String idRol) {
        this.idRol = idRol;
    }
}
