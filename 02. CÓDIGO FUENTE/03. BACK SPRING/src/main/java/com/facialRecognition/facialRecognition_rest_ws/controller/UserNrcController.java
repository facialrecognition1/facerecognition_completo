package com.facialRecognition.facialRecognition_rest_ws.controller;

import com.facialRecognition.facialRecognition_rest_ws.request.UserNrcRequest;
import com.facialRecognition.facialRecognition_rest_ws.service.UserNrcService;
import com.facialRecognition.facialRecognition_rest_ws.util.Path;
import jdk.jshell.Snippet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RestController
@RequestMapping(Path.USER_NRC)
public class UserNrcController {

    @Autowired
    UserNrcService userNrcService;

    @GetMapping
    public ResponseEntity getNrcByUserName (@RequestParam(value = "userName") Long userNrcId){
        return new ResponseEntity(userNrcService.getUserNrc(userNrcId), HttpStatus.OK);
    }

    @GetMapping(Path.ALL)
    public ResponseEntity getAllNrcByUserName(@RequestParam(value = "userName") String userName){
        return new ResponseEntity(userNrcService.getAllUserNrcs(userName), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity getAllNrcByUserName(@RequestBody UserNrcRequest userNrcRequest){
        return new ResponseEntity(userNrcService.addNrcToUser(userNrcRequest), HttpStatus.OK);
    }

    @PutMapping(value = Path.ID)
    public ResponseEntity updateNrcByUserNrcId(@PathVariable Long id, @RequestBody UserNrcRequest userNrcRequest){
        return new ResponseEntity(userNrcService.updateUserNrcByUserNrcId(id, userNrcRequest), HttpStatus.OK);
    }

    @DeleteMapping(value = Path.ID)
    public ResponseEntity deleteNrcByUserNrcId(@PathVariable Long id, @RequestParam Long userId){
        return new ResponseEntity(userNrcService.deleteUserNrcById(id, userId), HttpStatus.OK);
    }
}
