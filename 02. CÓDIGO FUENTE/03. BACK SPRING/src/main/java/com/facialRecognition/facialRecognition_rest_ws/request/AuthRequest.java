package com.facialRecognition.facialRecognition_rest_ws.request;

import lombok.Data;

@Data
public class AuthRequest {
    private String userName;
    private String password;
}
