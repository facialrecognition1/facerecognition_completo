package com.facialRecognition.facialRecognition_rest_ws.controller;

import com.facialRecognition.facialRecognition_rest_ws.request.AuthRequest;
import com.facialRecognition.facialRecognition_rest_ws.service.AuthService;
import com.facialRecognition.facialRecognition_rest_ws.util.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpResponse;

@RestController
@RequestMapping(value = "/authenticationUser")
public class AuthController {

    @Autowired
    AuthService authService;

    @GetMapping
    public ResponseEntity getAuthenticatedUser(@RequestBody AuthRequest authRequest){
        return new ResponseEntity(authService.authServiceValidation(authRequest), HttpStatus.OK);
    }
}
