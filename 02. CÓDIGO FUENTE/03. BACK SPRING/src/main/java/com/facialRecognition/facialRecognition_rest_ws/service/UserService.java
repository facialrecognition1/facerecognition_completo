package com.facialRecognition.facialRecognition_rest_ws.service;

import com.facialRecognition.facialRecognition_rest_ws.entity.User;
import com.facialRecognition.facialRecognition_rest_ws.repository.UserRepository;
import com.facialRecognition.facialRecognition_rest_ws.request.UserRequest;
import com.facialRecognition.facialRecognition_rest_ws.response.IUserResponse;
import com.facialRecognition.facialRecognition_rest_ws.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public IUserResponse getUserById(Long userId){
        IUserResponse userResponse = userRepository.getUserByUserId(userId);

        if(userResponse == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND);

        return userResponse;
    }

    public IUserResponse getUserByUserNameOrUniversityId(String userName, String universityId){
        IUserResponse userResponse = userRepository.findByUserNameOrUniversityId(userName, universityId);

        if(userResponse == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND);

        return userResponse;
    }

    public List<IUserResponse> getAllUsers() {
        List<IUserResponse> allUsers = userRepository.getAllUsers();
        if(allUsers == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND);
        return allUsers;
    }

    public static boolean validateEmail(String email) {
        String regex = ".*@espe\\.edu\\.ec$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public IUserResponse createNewUser(UserRequest userRequest){

        boolean userNameExist = userRepository.findByUserName(userRequest.getUserName()) != null;
        boolean emailExist = userRepository.findByEmail(userRequest.getEmail())!= null;
        boolean universityId = userRepository.findByUniversityId(userRequest.getUniversityId())!= null;
        boolean validEmail = validateEmail(userRequest.getEmail());

        if(userNameExist) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Message.USERNAME_ALREADY_EXIST);
        if(emailExist) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Message.EMAIL_ALREADY_EXIST);
        if(universityId) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Message.UNIVERSITY_ID_ALREADY_EXIST);
        if(!validEmail) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Message.INVALID_EMAIL);

        String encodedPassword = passwordEncoder.encode(userRequest.getPassword());

        User user = new User();
        user.setName(userRequest.getUserName());
        user.setEmail(userRequest.getEmail());
        user.setUniversityId(userRequest.getUniversityId());
        user.setPassword(encodedPassword);
        user.setUserName(userRequest.getUserName());
        user.setIdRol(userRequest.getIdRol());

        User saveUser = userRepository.save(user);
        IUserResponse response = userRepository.getUserByUserId(saveUser.getUserId());

        return response;
    }
    public IUserResponse editUser(Long userId, UserRequest userRequest){

        User user = userRepository.findById(userId).orElse(null);

        if(user == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND);
        boolean userNameExist = userRepository.findByUserName(userRequest.getUserName()) != null;
        boolean emailExist = userRepository.findByEmail(userRequest.getEmail())!= null;
        boolean universityId = userRepository.findByUniversityId(userRequest.getUniversityId())!= null;
        boolean validEmail = validateEmail(userRequest.getEmail());

        if(userNameExist) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Message.USERNAME_ALREADY_EXIST);
        if(emailExist) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Message.EMAIL_ALREADY_EXIST);
        if(universityId) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Message.UNIVERSITY_ID_ALREADY_EXIST);
        if(!validEmail) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Message.INVALID_EMAIL);

        user.setName(userRequest.getUserName());
        user.setEmail(userRequest.getEmail());
        user.setUniversityId(userRequest.getUniversityId());
        user.setPassword(userRequest.getPassword());
        user.setUserName(userRequest.getUserName());
        user.setIdRol(userRequest.getIdRol());

        User saveUser = userRepository.save(user);
        IUserResponse response = userRepository.getUserByUserId(saveUser.getUserId());

        return response;
    }

    public List<IUserResponse> deleteUserByUserId(Long userId) {
        User user = userRepository.findById(userId).orElse(null);

        if(user == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND);
        userRepository.delete(user);
        List<IUserResponse> userResponse = userRepository.getAllUsers();
        return userResponse;
    }
}
