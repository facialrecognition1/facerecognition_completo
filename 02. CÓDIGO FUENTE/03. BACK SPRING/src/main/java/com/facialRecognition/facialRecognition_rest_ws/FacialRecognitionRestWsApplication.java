package com.facialRecognition.facialRecognition_rest_ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class 	FacialRecognitionRestWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FacialRecognitionRestWsApplication.class, args);
	}

}
