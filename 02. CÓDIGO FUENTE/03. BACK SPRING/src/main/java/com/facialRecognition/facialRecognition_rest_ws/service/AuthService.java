package com.facialRecognition.facialRecognition_rest_ws.service;

import com.facialRecognition.facialRecognition_rest_ws.entity.User;
import com.facialRecognition.facialRecognition_rest_ws.repository.UserRepository;
import com.facialRecognition.facialRecognition_rest_ws.request.AuthRequest;
import com.facialRecognition.facialRecognition_rest_ws.response.IUserResponse;
import com.facialRecognition.facialRecognition_rest_ws.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class AuthService {

    @Autowired
    UserRepository userRepository;
    private BCryptPasswordEncoder passwordEncoder;

    public IUserResponse authServiceValidation(AuthRequest authRequest){
        User user = userRepository.findByUserName(authRequest.getUserName());

        if(user == null) throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, Message.AUTH_DECLINE);

        if (!authRequest.getPassword().equals(user.getPassword())) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, Message.AUTH_DECLINE);
        }

        IUserResponse response = userRepository.getUserByUserId(user.getUserId());
        return response;

    }
}
