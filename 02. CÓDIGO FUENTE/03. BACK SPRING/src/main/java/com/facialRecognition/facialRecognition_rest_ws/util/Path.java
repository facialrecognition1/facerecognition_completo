package com.facialRecognition.facialRecognition_rest_ws.util;

public class Path {
    public static final String ID = "/{id}";
    public static final String ALL = "/all";
    public static final String AUTH = "/auth";

    // USER
     public static final String USER = "/user";
     public static final String FIND_BY_USERNAME_UNIVERSITY_ID = "/find";

     // NRC
     public static final String NRC = "/nrc";
     public static final String NRC_SCHEDULE = "/schedule";

     // USER_NRC
    public static final String USER_NRC = "/userNrc";

}
