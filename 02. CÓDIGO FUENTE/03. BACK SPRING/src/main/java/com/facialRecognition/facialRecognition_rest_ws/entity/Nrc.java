package com.facialRecognition.facialRecognition_rest_ws.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "nrc")
public class Nrc {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_NRC")
    private Long nrcId;

    @Column(name = "descripcion")
    private String description;

    @Column(name = "curso")
    private String curse;

    @Column(name = "codigo_NRC")
    private String nrcCode;

    @Column(name = "id_horario")
    private Long scheduleId;

    public Nrc(Long nrcId, String description, String curse, String nrcCode, Long scheduleId) {
        this.nrcId = nrcId;
        this.description = description;
        this.curse = curse;
        this.nrcCode = nrcCode;
        this.scheduleId = scheduleId;
    }

    public Nrc() {
    }

    public Long getNrcId() {
        return nrcId;
    }

    public void setNrcId(Long nrcId) {
        this.nrcId = nrcId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurse() {
        return curse;
    }

    public void setCurse(String curse) {
        this.curse = curse;
    }

    public String getNrcCode() {
        return nrcCode;
    }

    public void setNrcCode(String nrcCode) {
        this.nrcCode = nrcCode;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }
}
