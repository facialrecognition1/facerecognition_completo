package com.facialRecognition.facialRecognition_rest_ws.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface INrcResponse {
    @JsonProperty("nrcId")
    Long getNrcId();

    @JsonProperty("description")
    String getDescription();

    @JsonProperty("curse")
    String getCurse();

    @JsonProperty("nrcCode")
    String getNrcCode();

    @JsonProperty("scheduleId")
    Long getScheduleId();

}
