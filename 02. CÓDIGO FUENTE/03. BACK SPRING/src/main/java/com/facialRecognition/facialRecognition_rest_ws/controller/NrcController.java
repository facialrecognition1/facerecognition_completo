package com.facialRecognition.facialRecognition_rest_ws.controller;

import com.facialRecognition.facialRecognition_rest_ws.request.NrcRequest;
import com.facialRecognition.facialRecognition_rest_ws.service.NrcService;
import com.facialRecognition.facialRecognition_rest_ws.util.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin
@RestController
@RequestMapping(Path.NRC)
public class NrcController {

    @Autowired
    NrcService nrcService;
    @PostMapping
    public ResponseEntity createNrc(@RequestParam("userId") Long userId, @RequestBody NrcRequest nrcRequest){
        return new ResponseEntity(nrcService.createNRC(userId, nrcRequest), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity getNrcByNrcCode(@RequestParam("nrcCode") String nrcCode){
        return new ResponseEntity(nrcService.getNrcByNrcCode(nrcCode), HttpStatus.OK);
    }

    @GetMapping(value = (Path.NRC_SCHEDULE + Path.ID))
    public ResponseEntity getNrcListByScheduleId(@PathVariable("id") Long id){
        return new ResponseEntity(nrcService.getAllNrcsByScheduleId(id), HttpStatus.OK);
    }
    @GetMapping(value = Path.ALL)
    public ResponseEntity getAllNrcs(){
        return new ResponseEntity(nrcService.getAllNrc(), HttpStatus.OK);
    }

    @PutMapping(value =Path.ID)
    public ResponseEntity updateNrc(@PathVariable String id, @RequestBody NrcRequest nrcRequest){
        return new ResponseEntity(nrcService.updateNrc(id, nrcRequest), HttpStatus.OK);
    }

}
