package com.facialRecognition.facialRecognition_rest_ws.repository;

import com.facialRecognition.facialRecognition_rest_ws.entity.Nrc;
import com.facialRecognition.facialRecognition_rest_ws.response.INrcResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NrcRepository  extends CrudRepository<Nrc, Long> {
    INrcResponse findByNrcId(Long nrcId);
    INrcResponse getNrcByNrcCode(String nrcCode);
    @Query("select n from Nrc n")
    List<INrcResponse> getAllNrcs();

    @Query("select n from Nrc n where n.scheduleId = :scheduleId")
    List<INrcResponse> getAllNrcsByScheduleId(Long scheduleId);

    Nrc findByNrcCode(String nrcCode);
}
